<?php
	include ('subnav_about.html');
?>

<h1>Our Goals</h1>

<h3>Improve Platform, Improve Process, Improve People</h3>

<h2>Our Commitment to Customers</h2>

<h3>Change and Configuration Management</h3>

<img src = "img/tilegroup_about/our_goals/change-and-config.png" style = "float:right; padding: 0 10px 10px 10px;" height = "100px">
<p align = "justify">Helps IT organizations to minimize the impact on business and mission objectives by managing real-time change, understanding asset dependencies, developing consistent and enforceable processes, and by integrating disparate data sources for single service views.</p>

<h3>Service Automation</h3>

<p align = "justify">Ensures that IT infrastructure dynamically adapts to meet business or mission demands while improving overall efficiency and reducing costs. Our solution allows IT organizations to manage game-changing technologies such as real-time infrastructure, cloud computing, and virtualization with well-defined processes and automation of complex tasks.</p>

<img src = "img/tilegroup_about/our_goals/service-automation.png" style = "float:right; padding: 0 10px 10px 10px;" height = "100px">
<h5 align = "justify">Infopath has been at the forefront of managing complex infrastructures for our large carrier, enterprise network, data center, and hosting provider clients for over a decade. Our unique vendor-agnostic position has made us a trusted partner for organizations that operate in diverse vendor environments. Our unique expertise allows us to leverage our client’s legacy investments to meet today’s challenge.</h5>

<h2>Why We Are Different?</h2>

<p align = "justify">Infopath offers a broad range of computer consulting services, from day-to-day user support to system maintenance and complete system engineering. Providing quality customer service has been a guiding principle at the firm since the beginning. Businesses depend on Infopath for not only solid technology expertise, but also for prompt and attentive service.</p>

<p align = "justify">Here’s what sets us apart from other consulting firms:</p>

<ul>
<li><p align = "justify">We are personable and professional. We hire engineers with the perfect mix of talents – they are technology experts who are also efficient and skilled in working with people from a variety of backgrounds. It is our profound conviction to keep the “people” side of consulting in sharp focus, above all else. We not only strive to find the best solutions to your computer problems, but we communicate these ideas and strategies in terms you can understand. We also dress professionally to suit your work environment.</p></li>
<li><p align = "justify">We are responsive. When you’re faced with a technical emergency, you can count on Infopath employees to be there to help promptly. Every client is assigned a primary and backup engineer, so you will know your contacts. We will not rotate staff through your company or change engineers at random.</p></li>
<li><p align = "justify">We are proactive. We help our clients minimize down times through preventative maintenance. Infopath aims to identify technical weaknesses and resolve them before they affect company productivity.</p></li>
</ul>
