<?php
	include ('subnav_about.html');
?>

<h1>Overview</h1>

<h2>Align business management priorities and metrics to IT operations</h2>

<table style = "float:right; padding:0 10px 10px 10px;">
<caption align="bottom">Choose the service best suited for you</caption>
<tr><td><img src="img/tilegroup_about/overview/benefits.png" width = "200px"/></td></tr>
</table>

<p align = "justify">Our Information technology service management (ITSM)  Process provides an extra edge through improved service quality, cost advantage and proactive IT management for delivering value-based IT services. The customizable service offering consists of:</p>

<ul>
<li>Rigorous Process Assessment</li>
<li>Documentation of Processes & their Validation</li>
<li>Optimized Process Implementation</li>
<li>Effective Process Management & Maintenance</li>
</ul>

<h2>Advantages of using Infopath</h2>

<p align = "justify">The key benefits derived by our customers are:<p>

<ul>
<li>Greater alignment of IT services, processes and goals with business requirements, expectations and goals</li>
<li>Improved business profitability and productivity</li>
<li>Reduction in overall management and support costs leading to a reduced TCO</li>
<li>Improved service availability and performance, leading to increased business revenue</li>
<li>Improved service levels and quality of service</li>
<li>Develop solutions for deploying and managing real-time infrastructure</li>
<li>Ensure high availability of services to end users</li>
</ul>
