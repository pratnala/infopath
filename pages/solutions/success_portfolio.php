<?php
	include ('subnav_solutions.html');
?>

<h1>Our Success Portfolio</h1>

<p align = "justify">Virtualization 3.0 where adaptive continuity takes flexible resource management to the next level. Hardware will provide more resilient infrastructure and instrumentation for enabling automation software to make the balancing decisions in real-time. Predictive decision making will readjust loads automatically based on changing workload requirements and/or data center demands, such as power, server resource changes, software failures, or other factors.</p>

<h2>Projects</h2>

<img src = "img/tilegroup_solutions/success_portfolio/people-process-partnerships.png" style = "float: right; padding: 0 10px 10px 10px" width = "140px">

<h3>People |  Process | Partnership</h3>

<p align = "justify">We implement our processes with a team that is highly experienced with Run Services techniques and methodology.  We also employ a service level agreement that keeps everyone on our team totally focused on providing you with outstanding service. Our objective is to ensure you get the most out of your outsourcing agreements, and that they are highly successful for both you and your Data Center provider.</p>

<img src = "img/tilegroup_solutions/success_portfolio/vmware-secure.png" style = "float: right; padding: 0 10px 10px 10px" width = "140px">

<h3>Secure Storage for VMware</h3>

<h4>Luke Hudson – Lead Engineer</h4>

<p align = "justify">The entire ESXi and iSCSI solution fit into one rack,” says Luke. “We didn’t need nearly as many physical servers, and with affordable, Ethernet-based SATA storage, I was able to meet the demands of my management when they said it has to cost under ‘X’.”</p>
 
<h3>Data Center Migration</h3>

<img src = "img/tilegroup_solutions/success_portfolio/data-center-migration.jpg" style = "float: right; padding: 0 10px 10px 10px" width = "140px">

<h4>Rod Jackson – Operations Manager</h4>

<p align = "justify">TMX removed seven legacy servers and upgraded and virtualized them onto four Dell PowerEdge rack-mount servers with Intel Xeon processors. “We saved so much power that we were able to cancel power circuits to quickly pay for the VMware project with savings on our monthly lease.</p>

<p align = "justify">With the Ethernet end-to end architecture Infopath proposed—which included Cisco switches inside the blade enclosures to minimize networking complexity—the cost picture improved dramatically.</p>
