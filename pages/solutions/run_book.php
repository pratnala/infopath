<?php
	include ('subnav_solutions.html');
?>

<h1>The Run Book</h1>

<h2>A Coping Mechanism for Data Center Complexity</h2>

<p align = "justify">For the uninitiated, a run book is an IT staff’s hard copy collection of system management cheat sheets. Data centers, of course, are like snowflakes - no two alike. Massive diversity of platforms, hardware and applications is the norm, as is some degree of specialization in the IT staff that manages systems, storage, network and front-line operations. Knowledge management is a chronic problem.</p>

<div style = "text-align: center; overflow: hidden;"><img src = "img/tilegroup_solutions/run_book/it-staff.jpg" style = "padding: 10px 10px 10px 10px" width = "850px"></div>

<h2>The Answer Is Automation</h2>

<p align = "justify">One obvious option is to automate as many procedures as is feasible and sensible. Given the choice between reading a lengthy instruction set of questionable reliability or running a script, most of us will instantly choose the latter, usually with better results. Automation can improve productivity, cut reaction times and reduce errors in many areas of Data Center operations.</p>

<p align = "justify">Various analysts have called out data center automation as an important strategy for meeting SLA commitments within tightening budgets and headcount constraints.</p>

<p align = "justify">IT decision makers have been advised to look for run book automation, process automation and orchestration tools that can be implemented incrementally, with the caveat that such tools must support all the diverse elements of the typically heterogeneous data center—physical and virtual machines, network devices, middle-ware, applications and data bases.</p>

<p align = "justify">We also integrate with all the existing management tools and processes already in place.</p>
