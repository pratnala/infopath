<?php
	include ('subnav_solutions.html');
?>

<h1>The Infopath Process</h1>

<h2>Plan | Build | Run</h2>

<img src = "img/tilegroup_solutions/process/plan-build-run.png" style = "float: right; padding: 0 10px 10px 10px" width = "200px">

<p align = "justify">Infopath has found that IT service management best practices, such as ITIL -- as well as automated IT asset management tools and automated event monitoring, correlation and root-cause analysis tools -- contribute to highly effective IT environments.</p>

<p align = "justify">"Effective IT management in today's complex data centers requires timely, accurate views of how servers, storage, networks, software and end-user systems are interacting," writes Mary Johnston Turner, senior market research analyst.</p>

<img src = "img/tilegroup_solutions/process/server-consolidation.png" style = "float: right; padding: 0 10px 10px 10px" width = "200px">

<h2>IT on the move – demand-driven</h2>

<p align = "justify">Initiatives such as server consolidation, virtualization, performance management and uptime and business continuity are driving the need for more automation and smarter shared services. "CIOs believe that by using policy and demand-driven automation they can meet these challenges and better respond to changing business needs," says Ben Scheerer.</p>

<h2>Smart People –  Need Smart process</h2>

<img src = "img/tilegroup_solutions/process/smart-people.jpg" style = "float: right; padding: 0 10px 10px 10px" width = "200px">

<p align = "justify">Enterprise Strategy Group's survey shows that if the process is not in place and it is not a repeatable process that can be mapped and measured, it would be very tough to put effective automation in place. A fool with a tool is still a fool.</p>
