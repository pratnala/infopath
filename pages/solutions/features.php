<?php
	include ('subnav_solutions.html');
?>

<h1>Key Features</h1>

<p align = "justify">Infopath offers some key features which differentiate us from others.</p>

<img src = "img/tilegroup_solutions/features/secure-business.jpg" style = "float: right; padding: 0 10px 10px 10px" width = "170px">

<h2>Secure the Business</h2>

<p align = "justify">For corporate environments, the overall goal is to allow users to run all applications, install key software and drivers, and run operating system features that make the company money, but to do this with the least privilege possible.  Our team knows how to protect you.</p>

<h2>Secure Endpoints</h2>

<img src = "img/tilegroup_solutions/features/secure-endpoints.jpeg" style = "float: right; padding: 0 10px 10px 10px" width = "170px">

<p align = "justify">Studies have shown that a locked down environment is more cost effective to support because the end users are less likely to make unnecessary changes to the core system configuration. A least privileges approach protects your distributed desktop environment against malware and malicious intent to change security settings and disable other security solutions. Implementing a locked down environment is also key in complying with various regulatory and compliance initiatives.</p>

<h2>Compliance Mandates</h2>

<img src = "img/tilegroup_solutions/features/compliance-mandates.jpg" style = "float: right; padding: 0 10px 10px 10px" width = "170px">

<p align = "justify">Many industries and corporations have strict regulations and corporate compliance policies based on regulations and requirements of the business. These policies are designed to safeguard the business and provide compliance to regulations and mitigate risk. Enforcement of these compliance policies and mandates can often be too restrictive at the desktop-level and in many cases interfere with worker productivity or cause an increase in IT support calls. This is primarily due to the fact that a proven method for organizations to secure desktops is to lock down its distributed desktop environment.</p>
