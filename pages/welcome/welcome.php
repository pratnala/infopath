<?php
	include ('subnav_welcome.html');
?>

<h1>Welcome to Infopath!</h1>

<h2>The situation today</h2>

<table style = "float:right; padding:0 10px 10px 10px;">
<caption align="bottom">Useful and cost-effective solutions are the need of the hour</caption>
<tr><td><img src="img/tilegroup_welcome/welcome/cost-effective.jpeg" height = "150px"/></td></tr>
</table>

<p align = "justify">For most enterprises, managing existing and new technologies throughout the enterprise is challenging under most circumstances, and the current economic downturn has further exacerbated the challenge by requiring solutions that are both useful and cost-effective. Meanwhile, with an ever-changing environment, the increasing complexities of technology, and a greater dependence on networked technologies, senior management and the lines of business are placing growing pressure on the IT organization to align IT services and business strategy for the greatest return on investment. Analysts report that infrastructure and operations can represent 60% to 70% of overall IT budget spending.</p>

<p align = "justify">Therefore, executives must implement a business technology management strategy that ensures all IT decisions maximize business value and span the organization, processes, technology, and information needs.</p>

<h2>Why Infopath?</h2>

<p align = "justify">Infopath offers a broad selection of data center products and services for those who may not have the specialized expertise necessary to effectively manage data center assets. Trust our state-of-the-art technology and services to keep your information protected and accessible.</p>

<p align = "justify">With Infopath, your business information and computer systems are secure, managed, and always running at high levels. Our data center services offer a unique combination of data security expertise based on ITIL best practices and disaster-proof data center management systems.</p>
