<?php
/*
All pages you have in the /pages/ folder must be defined here, it's their own title (shown on top and in the url)
ex: $pageTitles['file.php'] = "This is the title";
ex: $pageURL['file.php'] = "url";
Just copy paste this and fill in :
$pageTitles[''] = "";
*/
/* PAGE OPTIONS */

/* Group 0 - Welcome */

$pageTitles["welcome/welcome.php"] = "Welcome";
$pageTitles["welcome/partnerships.php"] = "Our Partnerships";
$pageTitles["welcome/getting_started.php"] = "Getting Started";

/* Group 1 - About Us */

$pageTitles["about/overview.php"] = "Overview";
$pageTitles["about/what_we_do.php"] = "What We Do";
$pageTitles["about/our_goals.php"] = "Our Goals";
$pageTitles["about/careers.php"] = "Careers";
$pageTitles["about/mission.php"] = "Mission";
$pageTitles["about/values.php"] = "Values";
$pageTitles["about/contact_us.php"] = "Contact Us";

/* Group 2 - Services */

$pageTitles["services/what_we_offer.php"] = "What We Offer";
$pageTitles["services/portfolio.php"] = "Our Portfolio";

/* Group 3 - Solutions */

$pageTitles["solutions/features.php"] = "Key Features";
$pageTitles["solutions/success_portfolio.php"] = "Success Portfolio";
$pageTitles["solutions/process.php"] = "Infopath Process";
$pageTitles["solutions/ssae.php"] = "SSAE";
$pageTitles["solutions/dcim.php"] = "Data Center Infrastructure Management";
$pageTitles["solutions/run_book.php"] = "Run Book";
$pageTitles["solutions/technology_lifecycle.php"] = "Technology Lifecycle";

/* Page url, this will be visible in the url bar, for example if you fill in "welcome" the url for that page will become yoursite.com/#!/welcome */

/* Group 0 - Welcome */

$pageURL["welcome/welcome.php"] = "welcome";
$pageURL["welcome/partnerships.php"] = "partnerships";
$pageURL["welcome/getting_started.php"] = "getting_started";

/* Group 1 - About Us */

$pageURL["about/overview.php"] = "overview";
$pageURL["about/what_we_do.php"] = "what_we_do";
$pageURL["about/our_goals.php"] = "our_goals";
$pageURL["about/careers.php"] = "careers";
$pageURL["about/mission.php"] = "mission";
$pageURL["about/values.php"] = "values";
$pageURL["about/contact_us.php"] = "contact_us";

/* Group 2 - Services */

$pageURL["services/what_we_offer.php"] = "what_we_offer";
$pageURL["services/portfolio.php"] = "portfolio";

/* Group 3 - Solutions */

$pageURL["solutions/features.php"] = "features";
$pageURL["solutions/success_portfolio.php"] = "success_portfolio";
$pageURL["solutions/process.php"] = "process";
$pageURL["solutions/ssae.php"] = "ssae";
$pageURL["solutions/dcim.php"] = "dcim";
$pageURL["solutions/run_book.php"] = "run_book";
$pageURL["solutions/technology_lifecycle.php"] = "technology_lifecycle";

/* Optional: page descriptions (for Google)*/
// $pageDescriptions['welcome.php'] = "This is the welcome page. Welcome.";

/* Optional: page keywords (for Google)(isn't important at all)*/
// $pageKeywords['welcome.php'] = "welcome, page, test";
	
?>