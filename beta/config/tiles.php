<?php
/* All tiles on the homepage are configured here, be sure to check the tutorials/docs on http://metro-webdesign.info */

/* Group 0 - Welcome */

$tile[] = array("type" => "img", "group" => 0, "x" => 0, "y" => 0, "width" => 1, "height" => 1, "url" => "welcome/welcome.php", "img" => "img/tilegroup_welcome/welcome.gif", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Welcome to Infopath!</span></div>", "showDescAlways" => true, "imgWidth" => 1, "imgHeight" => 1, "labelPosition" => "bottom");

$tile[] = array("type" => "img", "group" => 0, "x" => 1, "y" => 0, "width" => 2, "height" => 1, "url" => "welcome/partnerships.php", "img" => "img/tilegroup_welcome/partnerships.jpg", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Our Partnerships</span></div>", "showDescAlways" => true, "imgWidth" => 2, "imgHeight" => 1, "labelPosition" => "bottom");

$tile[] = array("type" => "scrollText", "group" => 0, "x" => 0, "y" => 1, "width" => 3, "height" => 1, "url" => "welcome/getting_started.php", "img" => "img/tilegroup_welcome/getting-started.jpg", "imgSize" => 145, "imgToLeft" => 0, "imgToTop" => 0, "background" => "#877121", "title" => "Getting Started", "text" => array("<span style = 'font-size: 17px;'>We believe in customer-focused services</span>","<span style = 'font-size: 17px;'>Know more about how Infopath can understand your business and fit your needs</span>", "<span style = 'font-size: 17px;'>Click here</span>"), "scrollSpeed" => 3000);

/* Group 1 - About Us */

$tile[] = array("type" => "img", "group" => 1, "x" => 0, "y" => 0, "width" => 1, "height" => 1, "url" => "about/overview.php", "img" => "img/tilegroup_about/overview.jpg", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Overview</span></div>", "showDescAlways" => true, "imgWidth" => 1, "imgHeight" => 1, "labelPosition" => "bottom");

$tile[] = array("type" => "img", "group" => 1, "x" => 1, "y" => 0, "width" => 2, "height" => 1, "url" => "about/what_we_do.php", "img" => "img/tilegroup_about/what-we-do.jpg", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Find out more about what we do!</span></div>", "showDescAlways" => true, "imgWidth" => 2, "imgHeight" => 1, "labelPosition" => "bottom");

$tile[] = array("type" => "scrollText", "group" => 1, "x" => 0, "y" => 1, "width" => 2, "height" => 1, "url" => "about/our_goals.php", "img" => "img/tilegroup_about/goal.png", "imgSize" => 145, "imgToLeft" => 0, "imgToTop" => 0, "background" => "#4353AD", "title" => "Our Goals", "text" => array("<span style = 'font-size: 17px;'>Improve Platform</span>","<span style = 'font-size: 17px;'>Improve Process</span>", "<span style = 'font-size: 17px;'>Improve People</span>"), "scrollSpeed" => 3000);

$tile[] = array("type" => "img", "group" => 1, "x" => 2, "y" => 1, "width" => 1, "height" => 1, "url" => "about/contact_us.php", "img" => "img/tilegroup_about/contact-us.jpg", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Click here to find out ways to reach us!</span></div>", "showDescAlways" => true, "imgWidth" => 1, "imgHeight" => 1, "labelPosition" => "bottom");

$tile[] = array("type" => "img", "group" => 1, "x" => 0, "y" => 2, "width" => 1, "height" => 1, "url" => "about/mission.php", "img" => "img/tilegroup_about/mission.jpg", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Our Mission</span></div>", "showDescAlways" => true, "imgWidth" => 1, "imgHeight" => 1, "labelPosition" => "bottom");

$tile[] = array("type" => "img", "group" => 1, "x" => 1, "y" => 2, "width" => 1, "height" => 1, "url" => "about/values.php", "img" => "img/tilegroup_about/values.gif", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Our Value System</span></div>", "showDescAlways" => true, "imgWidth" => 1, "imgHeight" => 1, "labelPosition" => "bottom");

$tile[] = array("type" => "img", "group" => 1, "x" => 2, "y" => 2, "width" => 1, "height" => 1, "url" => "about/careers.php", "img" => "img/tilegroup_about/careers.jpg", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Careers @ Infopath</span></div>", "showDescAlways" => true, "imgWidth" => 1, "imgHeight" => 1, "labelPosition" => "bottom");

/* Group 2 - Services */

$tile[] = array("type" => "img", "group" => 2, "x" => 0, "y" => 0, "width" => 1, "height" => 1, "url" => "services/what_we_offer.php", "img" => "img/tilegroup_services/what-we-offer.jpg", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Know more about the services we offer</span></div>", "showDescAlways" => true, "imgWidth" => 1, "imgHeight" => 1, "labelPosition" => "bottom");

$tile[] = array("type" => "img", "group" => 2, "x" => 1, "y" => 0, "width" => 2, "height" => 1, "url" => "services/portfolio.php", "img" => "img/tilegroup_services/portfolio.jpg", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Our Portfolio</span></div>", "showDescAlways" => true, "imgWidth" => 2, "imgHeight" => 1, "labelPosition" => "bottom");

/* Group 3 - Solutions */

$tile[] = array("type" => "img", "group" => 3, "x" => 0, "y" => 0, "width" => 1, "height" => 1, "url" => "solutions/features.php", "img" => "img/tilegroup_solutions/key-features.jpg", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Key Features</span></div>", "showDescAlways" => true, "imgWidth" => 1, "imgHeight" => 1, "labelPosition" => "bottom");

$tile[] = array("type" => "img", "group" => 3, "x" => 1, "y" => 0, "width" => 2, "height" => 1, "url" => "solutions/success_portfolio.php", "img" => "img/tilegroup_solutions/success-portfolio.jpg", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Know more about our success portfolio!</span></div>", "showDescAlways" => true, "imgWidth" => 2, "imgHeight" => 1, "labelPosition" => "bottom");

$tile[] = array("type" => "img", "group" => 3, "x" => 0, "y" => 1, "width" => 1, "height" => 1, "url" => "solutions/process.php", "img" => "img/tilegroup_solutions/process.jpg", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Process</span></div>", "showDescAlways" => true, "imgWidth" => 1, "imgHeight" => 1, "labelPosition" => "bottom");

$tile[] = array("type" => "img", "group" => 3, "x" => 1, "y" => 1, "width" => 1, "height" => 1, "url" => "solutions/ssae.php", "img" => "img/tilegroup_solutions/ssae.jpg", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>SSAE 16 Information</span></div>", "showDescAlways" => true, "imgWidth" => 1, "imgHeight" => 1, "labelPosition" => "bottom");

$tile[] = array("type" => "img", "group" => 3, "x" => 2, "y" => 1, "width" => 1, "height" => 1, "url" => "solutions/dcim.php", "img" => "img/tilegroup_solutions/dcim.jpg", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Data Center Infrastructure</span></div>", "showDescAlways" => true, "imgWidth" => 1, "imgHeight" => 1, "labelPosition" => "bottom");

$tile[] = array("type" => "simple", "group" => 3, "x" => 0, "y" => 2, "width" => 2, "height" => 1, "url" => "solutions/run_book.php", "background" => "#AA1010", "img" => "img/tilegroup_solutions/datacenter.jpg", "imgSize" => 145, "imgToTop" => 0, "imgToLeft" => 0, "title" => "Run Book", "text" => "<span style = 'font-size: 14px;'>Know how we cope with data center complexity</span>");

$tile[] = array("type" => "img", "group" => 3, "x" => 2, "y" => 2, "width" => 1, "height" => 1, "url" => "solutions/technology_lifecycle.php", "img" => "img/tilegroup_solutions/lifecycle.jpg", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Technology Lifecycle</span></div>", "showDescAlways" => true, "imgWidth" => 1, "imgHeight" => 1, "labelPosition" => "bottom");

/* Group 4 - Support */

$tile[] = array("type" => "img", "group" => 4, "x" => 0, "y" => 0, "width" => 1, "height" => 1, "url" => "", "img" => "img/tilegroup_support/support.jpg", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Welcome to Support</span></div>", "showDescAlways" => true, "imgWidth" => 1, "imgHeight" => 1, "labelPosition" => "bottom", "classes" => "noClick");

$tile[] = array("type" => "img", "group" => 4, "x" => 1, "y" => 0, "width" => 1, "height" => 1, "url" => "", "img" => "img/tilegroup_support/remote-access.png", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Remote  Services</span></div>", "showDescAlways" => true, "imgWidth" => 1, "imgHeight" => 1, "labelPosition" => "bottom", "classes" => "noClick");

$tile[] = array("type" => "img", "group" => 4, "x" => 2, "y" => 0, "width" => 1, "height" => 1, "url" => "", "img" => "img/tilegroup_support/live-chat.jpg", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Live Chat</span></div>", "showDescAlways" => true, "imgWidth" => 1, "imgHeight" => 1, "labelPosition" => "bottom", "classes" => "noClick");

$tile[] = array("type" => "img", "group" => 4, "x" => 0, "y" => 1, "width" => 2, "height" => 1, "url" => "", "img" => "img/tilegroup_support/data-center.jpg", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Onsite Data Center Services</span></div>", "showDescAlways" => true, "imgWidth" => 2, "imgHeight" => 1, "labelPosition" => "bottom", "classes" => "noClick");

$tile[] = array("type" => "img", "group" => 4, "x" => 2, "y" => 1, "width" => 1, "height" => 1, "url" => "", "img" => "img/tilegroup_support/conference.gif", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Video-conferencing</span></div>", "showDescAlways" => true, "imgWidth" => 1, "imgHeight" => 1, "labelPosition" => "bottom", "classes" => "noClick");

$tile[] = array("type" => "img", "group" => 4, "x" => 0, "y" => 2, "width" => 1, "height" => 1, "url" => "", "img" => "img/tilegroup_support/policy.jpg", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Support Policy</span></div>", "showDescAlways" => true, "imgWidth" => 1, "imgHeight" => 1, "labelPosition" => "bottom", "classes" => "noClick");

$tile[] = array("type" => "img", "group" => 4, "x" => 1, "y" => 2, "width" => 2, "height" => 1, "url" => "", "img" => "img/tilegroup_support/vpn.png", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>VPN Connection to Support</span></div>", "showDescAlways" => true, "imgWidth" => 2, "imgHeight" => 1, "labelPosition" => "bottom", "classes" => "noClick");

/* Group 5 - Contact */

$tile[] = array("type" => "img", "group" => 5, "x" => 0, "y" => 0, "width" => 1, "height" => 1, "url" => "external:mailto:info@infopath.net", "img" => "img/tilegroup_contact/email.png", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Email us</span></div>", "showDescAlways" => true, "imgWidth" => 1, "imgHeight" => 1, "labelPosition" => "bottom");

$tile[] = array("type" => "img", "group" => 5, "x" => 1, "y" => 0, "width" => 1, "height" => 1, "url" => "https://www.facebook.com/pages/Infopath/168955483142204", "img" => "img/tilegroup_contact/facebook.png", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Like us</span></div>", "showDescAlways" => true, "imgWidth" => 1, "imgHeight" => 1, "labelPosition" => "bottom");

$tile[] = array("type" => "img", "group" => 5, "x" => 0, "y" => 1, "width" => 1, "height" => 1, "url" => "https://twitter.com/infopath1", "img" => "img/tilegroup_contact/twitter.png", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Tweet us</span></div>", "showDescAlways" => true, "imgWidth" => 1, "imgHeight" => 1, "labelPosition" => "bottom");

$tile[] = array("type" => "img", "group" => 5, "x" => 1, "y" => 1, "width" => 1, "height" => 1, "url" => "external:skype:infopathcorp?call", "img" => "img/tilegroup_contact/skype.png", "desc" => "<div style = 'text-align: center;'><span style = 'font-size: 14px;'>Skype us</span></div>", "showDescAlways" => true, "imgWidth" => 1, "imgHeight" => 1, "labelPosition" => "bottom");

?>
