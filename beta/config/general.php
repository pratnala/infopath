<?php
date_default_timezone_set ('America/New_York');
/* GENERAL CONFIG */

$siteTitle = 'Infopath'; /* will be displayed above the url-bar / in tab / on Google */
$siteName = 'Infopath'; /* The biggest title on your homepage */
$siteDesc ='Data Center Service Assurance'; /* subtitle on your homepage */
$siteTitleHome = 'Data Center Service Assurance'; // will be displayed above the url-bar / in tab / in google when the home-page is open
$siteFooter = '<div style = "text-align: center;"><strong>© 2001 - ' . date ("Y") . ' Infopath. All Rights Reserved</strong>' . '<br />' . 'Last updated: ' . date("l, jS F, Y") . '</div>';

$siteMetaDesc = 'Infopath is uniquely and exclusively focused on helping our clients make smarter decisions about enhancing their Information Technology solutions.';
$siteMetaKeywords = 'infopath, data center, services';

$favicon_url = "infopath.ico";

$googleAnalyticsCode = ""; // Your Google Analytics Web Property ID in the form UA-XXXXX-Y or UA-XXXXX-YY. (check: http://support.google.com/analytics/bin/answer.py?hl=en&answer=1032385)

$lang = "en"; // lang of the site (used for locale file! So if you put "nl" you need a file nl.php in the locale folder. (to create such a file: start by copying the default en.php file)

/* Compressing settings */
$compressJS = false; // compress JS
$compressCSS = false; // compress CSS
$autoFlush = true;
$autoFlushPlugins = true;

$compressJS_mob = false; // compress JS of mobile site
$compressCSS_mob = false; // compress CSS of mobile site
$autoFlush_mob = true;
$autoFlushPlugins_mob = true;

/* Plugin settings*/
$disabledPluginsDesktop = array(); // add the folder names here if you want to specifically disable plugins on the main (full/desktop) site
$disabledPluginsMobile = array(); // add the folder names here if you want to specifically disable plugins on the mobile site

?>
