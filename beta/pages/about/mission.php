<?php
include_once("subnav.php");
include_once("../../inc/essentials.php");
?>

<script>
	$mainNav.set("About Us");
</script>

<h1>Mission</h1>

<p align = "justify">Our customer&rsquo;s mission is our mission. We will see mission challenges through our customer&rsquo;s eyes and provide the highest quality, best-value solutions possible through our employees, partners, and proven technology.</p>

<img src = "img/tilegroup_about/mission/mission-statement.jpg" style = "float:right; padding: 0 10px 10px 10px;" width = "150px">

<p align = "justify">Infopath accomplishes its mission by:</p>

<ul>
<li> Using commercial best practices and innovations to provide the highest level of IT solutions</li>
<li> Providing dynamic transformation in alignment with each customer's mission, strategies, and vision</li>
<li> Approaching our responsibilities with the intensity expected of a mission-critical IT provider</li>
<li> Working within a framework of people, processes and technologies</li>
</ul>

<h2>From Vision to Action</h2>

<img src = "img/tilegroup_about/mission/vision.png" style = "float:right; padding: 0 10px 10px 10px;" width = "150px">

<p align = "justify">Infopath is guided not only by our mission, but also by our values, strategic goals, company goals, and our commitment to measurement and continuous improvement. Our senior management team comes together on a regular basis to assess progress, readjust tactics to align with goals and raise the bar for the quality of our work, the expertise of our workforce, and the relationships we forge with our customers and partners. This ongoing oversight is critical to successful outcomes for our customers and for Infopath.</p>

<h2>Values</h2>

<ul>
<li><p align = "justify"><strong>Integrity:</strong> To act at all times in an honest and ethical manner, with the understanding that our actions, as individuals and as corporate citizens, enhance the quality of life in the communities and organizations in which we do business.</p></li>
<li><p align = "justify"><strong>Fairness:</strong> To treat each other, our customers, competitors, suppliers, and members of the community with respect, and to do the right thing no matter what.</p></li>
<img src = "img/tilegroup_about/mission/core-values.png" style = "float:right; padding: 0 10px 10px 10px;" width = "150px">
<li><p align = "justify"><strong>Respect:</strong> To value the contributions of every individual and to recognize the importance of diversity and change.</p></li>
<li><p align = "justify"><strong>Initiative:</strong> To identify obstacles to success and create solutions. We hold ourselves accountable for meeting our commitments. We take calculated risks to achieve our goals. We pursue excellence, as individuals and as a corporation, in everything we do.</p></li>
<li><p align = "justify"><strong>Social Responsibility:</strong> To participate in the communities within which we work and to continually strive to make a positive impact on the individuals and environment that support us.</p></li>
</ul>