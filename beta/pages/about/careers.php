<?php
include_once("subnav.php");
include_once("../../inc/essentials.php");
?>

<script>
	$mainNav.set("About Us");
</script>

<h1>Careers @ Infopath</h1>

<p align = "justify">Infopath is always looking for talented individuals to join our team.</p>

<h2>The Infopath Advantage</h2>

<p align = "justify">Infopath offers a full range of flexible options to accommodate the needs of our diverse employees and their families including:</p>

<ul>
<img src = "img/tilegroup_about/careers/advantage.png" style = "float:right;" width = "200px" />
<li><p align = "justify">Comprehensive health, dental and vision coverage plans</p></li>
<li><p align = "justify">Paid personal time off</p></li>
<li><p align = "justify">Flexible spending accounts</p></li>
<li><p align = "justify">Wellness Program</p></li>
<li><p align = "justify">401k with company match</p></li>
<li><p align = "justify">Income protection programs</p></li>
<li><p align = "justify">Training: a wide variety of professional development programs, including online training 24x7 from a catalog of 3,000+ courses plus partnerships with University of Phoenix and University of Maryland</p></li>
<li><p align = "justify">Tuition reimbursement</p></li>
<li><p align = "justify">Bonus and employee recognition programs</p></li>
<li><p align = "justify">Flexible </p></li>
</ul>

<h2>Solve critical technology challenges</h2>

<p align = "justify">At Infopath, your job is more than a paycheck. As a member of our team, your expertise is used to support the IT and strategic solutions we provide to our customers. Our services range from strategic consulting to managed flexible services in five business areas: Cloud Services, Data Security, Data Center Services,  and IT Consolidaton Solutions.</p>

<p align = "justify"> Do you have what it takes to solve critical technology challenges? If yes, we're looking for you. We care about our employees, and strive to create the best work environment and career opportunities for our staff to develop professionally and advance in their careers.</p>