<?php
include_once("subnav.php");
include_once("../../inc/essentials.php");
?>

<script>
	$mainNav.set("About Us");
</script>

<h1>What we Do</h1>

<h2>Who We Are – Problem Solvers</h2>

<img src = "img/tilegroup_about/what_we_do/who-we-are.png" style = "float: right; padding: 0 0 0 10px;" width = "120px">
<p align = "justify">Transforming problems into solutions is what we're all about. And we're very good at it. No matter how unique or complex the problem, if it's possible, we'll solve it. Large or small, whether you need help with business operations or processes, storage or security, we have a solution that's right for you. Our Information Technology Service Management (ITSM) process provides an extra edge through improved service quality, cost advantage and proactive IT management for delivering value-based IT services.</p>

<h2>We think differently – Anytime anywhere</h2>

<img src = "img/tilegroup_about/what_we_do/think-different.png" style = "float: right; padding: 0 0 0 10px;" width = "120px">
<p align = "justify">A world where we can unlock your maximum potential through IT, where everything you need is at your fingertips, the way you want it, wherever you are; in the office, at home, or on the move. We also believe our business should provide the best environment possible for our people, minimise our environmental impact and play a positive integral role in the communities with which we share our home</p>

<h2>People and Partnerships</h2>

<img src = "img/tilegroup_about/what_we_do/people-partnerships.jpg" style = "float:right; padding: 0 0 0 10px;" width = "120px">
<p align = "justify">As people are our primary asset, we recognise that the future of our business depends on them. That’s why we’re whole-heartedly committed to the ongoing development of individual talent as an essential part of the wider Infopath team. We partner with some of the world's leading technology vendors and provide significant added value to their products.</p>

<h2>Long Term Way of Life</h2>

<img src = "img/tilegroup_about/what_we_do/long-term.png" style = "float: right; padding: 0 0 0 10px;" width = "120px">
<p align = "justify">Since we began in 2001, Infopath brings a solid track record in helping clients align IT operations with real business priorities. We are trusted by top US Fortune 1000 businesses and high-profile military and Federal agencies to deliver straightforward guidance, deep operational and process knowledge, and unbiased, product-agnostic approaches to IT operational excellence.</p>
