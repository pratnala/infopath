<?php
include_once("subnav.php");
include_once("../../inc/essentials.php");
?>

<script>
	$mainNav.set("About Us");
</script>

<h1>Contact Us</h1>

<p align = "justify">Our postal address is as follows:</p>

<h5><strong>6825 Shiloh Road East<br />
Alpharetta<br />
Georgia 30005-2227</strong></h5>

<img src = "img/tilegroup_about/contact_us/phone.png" style = "float: right; padding: 0 10px 10px 10px;" width = "200px" />
<p align = "justify">You can reach us via phone at:</p>

<h5><strong>(866) 871 - 2674</strong></h5>

<p align = "justify">Email us at:</p>

<h5><strong><a href = "mailto:info@infopath.net">info@infopath.net</a></strong></h5>

<p align = "justify">Infopath is now on social media as well!</p>

<h5><strong>Facebook: <a href="https://www.facebook.com/pages/Infopath/168955483142204">https://www.facebook.com/pages/Infopath/168955483142204</a></strong></h5>

<h5><strong>Twitter: <a href="https://twitter.com/infopath1">https://twitter.com/infopath1</a></strong></h5>
