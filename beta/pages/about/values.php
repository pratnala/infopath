<?php
include_once("subnav.php");
include_once("../../inc/essentials.php");
?>

<script>
	$mainNav.set("About Us");
</script>

<h1>The Infopath Value System</h1>

<h2>Our Clients</h2>

<img src = "img/tilegroup_about/values/clients.png" style = "float:right;" width = "200px" />

<p align = "justify">Our clients, employees and our community recognize us a a firm with:</p>

<ul>
<li><p align = "justify">Corporate culture centered on the core values of professionalism, fairness, integrity, trust, diversity, excellence, and teamwork</p></li>
<li><p align = "justify">Leaders who combine vision and practical experience with integrity</p></li>
<li><p align = "justify">Commitment to supporting diversity in our workforce</p></li>
<li><p align = "justify">Obsession with quality</p></li>
</ul>

<p align = "justify">These values differentiate Infopath from the many other IT firms in the market today. Our values underpin our relationships with our staff, clients, partners and the community. We trust our staff to make decisions and to act based on values. Infopath values are not just words, but embody everything we do. Our clients are our most important aspect in our business and we constantly consider that in everything that we do. The entire organization has a deep commitment to exceeding client expectations and delivering value in all work we perform.</p>

<h2>Our Team</h2>

<p align = "justify">Infopath attracts a diverse group of extremely talented, motivated, and innovative professionals. Then, we provide them with a variety of learning and networking opportunities – including speaking, publishing, and working with top leaders – to help accelerate their careers. Our work is important, with amazing colleagues, in a cohesive firm dedicated to our clients. You&rsquo;ll be among people who share commitment to helping customers, and delivering quality in everything that they do. Our core values and tradition of service—offers an excellent environment for your aspirations and experience.</p>
<p align = "justify">We strive for learning, balanced work-life integration, and recognition for your achievements. Your innovation can take root among a talented team of thought leaders and high achievers—dedicated to excellence, passionate about results, and committed to impact and growth. There&rsquo;s room to develop at Infopath where individual action leads to firm-wide impact and that impact leads to growth.</p>
<p align = "justify">Infopath is the world&rsquo;s best cloud computing consulting company. Collaborate with like-minded people in an environment that embraces diversity and rewards your best work. Deepen your expertise and learn from the insights of other consultants at the top of their field and other industry experts. Develop a diverse and esteemed network of  connections that will propel your career for a lifetime!</p>
