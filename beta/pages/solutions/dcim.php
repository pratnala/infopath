<?php
include_once("subnav.php");
include_once("../../inc/essentials.php");
?>

<script>
	$mainNav.set("Solutions");
</script>

<h1>Data Center Infrastructure Management</h1>

<h2>Overview</h2>

<p align = "justify">Demand for new and more powerful IT-based applications, combined with the economic benefits of consolidation of physical assets, has led to an unprecedented expansion of data centers in both size and density.  Limitations of space and power, along with the enormous complexity of managing a large data center, have given rise to a new category of tools with integrated processes – Data Center Infrastructure Management (DCIM).</p>

<p align = "justify">Once properly deployed, a comprehensive DCIM solution provides data center operations managers with clear visibility of all data center assets along with their connectivity and relationships to support infrastructure – networks, copper and fiber cable plants, power chains and cooling systems. DCIM tools provide data center operations managers with the ability to identify, locate, visualize and manage all physical data center assets, simply provision new equipment and confidently plan capacity for future growth and/or consolidation.</p>

<p align = "justify">Basic DCIM components and functions include:</p>

<ul>
<li><p align = "justify"><strong>A Single Repository:</strong> One accurate, authoritative database to house all data from across all data centers and sites of all physical assets, including data center layout, with detailed data for IT, power and HVAC equipment and end-to-end network and power cable connections.</p></li>
<li><p align = "justify"><strong>Asset Discovery and Asset Tracking:</strong> Tools to capture assets, their details, relationships and inter-dependencies.</p></li>
<li><p align = "justify"><strong>Visualization:</strong> Graphical visualization, tracking and management of all data center assets and their related physical and logical attributes – servers, structured cable plants, networks, power infrastructure and cooling equipment.</p></li>
<li><p align = "justify"><strong>Provisioning New Equipment:</strong> Automated tools to support prompt and reliable deployment of new systems and all their related physical and logical resources.</p></li>
<li><p align = "justify"><strong>Real-Time Data Collection:</strong> Integration with real-time monitoring systems to collect actual power usage/environmental data to optimize capacity management, allowing review of real-time data vs.assumptions around nameplate data.</p></li>
<li><p align = "justify"><strong>Process-Driven Structure:</strong> Change management workflow procedures to ensure complete and accurate adds, changes and moves.</p></li>
<li><p align = "justify"><strong>Capacity Planning:</strong> Capacity planning tools to determine requirements for future floor and rack space, power, cooling expansion, what-if analysis and modeling.</p></li>
<li><p align = "justify"><strong>Reporting:</strong> Simplified reporting to set operational goals, measure performance and drive improvement.</p></li>
<li><p align = "justify"><strong>A Holistic Approach:</strong> Bridge across organizational domains – facilities, networking and systems, filling all functional gaps; used by all data center domains</p></li>
</ul>
<div style = "text-align: center; overflow: hidden;"><img src = "img/tilegroup_solutions/dcim/dcim.png" style = "padding: 10px 10px 10px 10px" width = "850px"></div>

<p align = "justify">Once the initial DCIM asset database is built, the entire staff must follow best practices for change management to maintain a complete and accurate repository, otherwise the DCIM’s value will begin to deteriorate and the system will fall into disuse. Most DCIM vendors provide computer-based tools to facilitate and enforce these processes to expedite workflow and maintain database accuracy. A structured change and work management process can coordinate and track the efforts of the data center staff as they install, configure and provision new services. This provides the tactical “glue” to unite the various data center domains.</p>
