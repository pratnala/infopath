<?php
include_once("subnav.php");
include_once("../../inc/essentials.php");
?>

<script>
	$mainNav.set("Solutions");
</script>

<h1>SSAE 16</h1>

<h2>SSAE = Service Assurance</h2>

<p align = "justify">"SSAE 16 helps us illustrate to our clients that we employ sound operational processes and controls in both business processing services and technology management" -  David Winters</p>

<p align = "justify">As a growing company, the Infopath team understood the importance of maintaining solid operating policies. We are committed to continually enhancing the effectiveness of our operations in order to meet or exceed industry standards and instill confidence amongst our customers and business partners.</p>

<h2>Statement on Standards for Attestation Engagements</h2>

<p align = "justify">Statement on Standards for Attestation Engagements 16 (SSAE 16), formally titled “Reporting on Controls at a Service Organization”, does not significantly overhaul the reporting process but does include some noteworthy changes, which include:</p>

<ul>
<li><p align = "justify">An increased focus on the proper application of the standard and use of the report</p></li>
<li><p align = "justify">Modifications to the form and content of the previous SAS 70 reporting format, including the concepts of specified criteria and management’s assertion</p></li>
<li><p align = "justify">Extension of requirements to sub-service organizations that are likely to increase the application of the “carve-out” reporting method</p></li>
</ul>

<p align = "justify">The AICPA recently introduced a Service Organization Controls (SOC) reporting structure consisting of three types of reports, including SSAE 16.</p>

<img src = "img/tilegroup_solutions/ssae/yearly-audit.png" style = "float:right; padding: 0 10px 10px 10px;" width = "150px"/>
<p align = "justify">As part of yearly audit, a number of Infopath’s operating controls were tested. These controls include: environment security, data communications and transmission, application development, change management, disaster recovery preparedness, support operations, and more. The audit examined both the suitability of the design and operating effectiveness of each control, and showed all to be in compliance.</p>

<h2>SSAE News</h2>

<img src = "img/tilegroup_solutions/ssae/expertise.gif" style = "float:right; padding: 0 10px 10px 10px;" width = "150px"/>
<p align = "justify">In April 2010, the AICPA issued Statement on Standards for Attestation Engagements (SSAE) 16, Reporting on Controls at a Service Organization, as the substantially equivalent US attestation standard to the new international standard for service organization reporting, International Standard on Assurance Engagements (ISAE) 3402. The international standard used the existing SAS 70 audit standard as its basis and incorporated changes. SSAE 16 uses ISAE 3402 as its basis, but includes some relatively minor differences. Both of the new reporting standards are very similar to the existing SAS 70 standard.</p>

<h2>Trust Service Principles</h2>

<table>
<tr>
<td><em>Security</em></td><td>The system is protected against unauthorized access (both physical and logical).</td>
</tr>
<tr>
<td><em>Availability</em></td><td>The system is available for operation and use as committed or agreed.</td>
</tr>
<tr>
<td><em>Processing Integrity</em></td><td>System processing is complete, accurate, timely, and authorized.</td>
</tr>
<tr>
<td><em>Confidentiality</em></td><td>Information designated as confidential is protected as committed or agreed.</td>
</tr>
<tr>
<td><em>Privacy</em></td><td>Personal information is collected, used, retained, disclosed, and destroyed in conformity with the commitments in the entity’s privacy notice and with criteria set forth in generally accepted privacy principles (GAPP) issued by the AICPA and CICA.</td>
</tr>
</table>

<h2>SAS 70 and the SSAE 16</h2>

<p align = "justify">The SSAE 16 has many similarities to the current SAS 70 standard, but has updated rules and requirements in the vein of the international standard, the ISAE 3402. The key difference between the SAS 70 and the SSAE 16 is the requirement for management to write their own assessment of the controls and provide a written assertion regarding the controls, which the 3rd party auditor must attest to by performing testing procedures. The testing procedures included onsite inquiry, observation and inspection of the controls by the A-lign auditor.</p>

<p align = "justify">The SSAE 16 standard places more emphasis on the service provider having a basis for their written assertion regarding the operational effectiveness of their controls. Infopath’s control objectives are in the following areas; Data Center Security, Environmental Safeguards, Preventive Maintenance, Network Availability, and ITIL compliant customer Ticketing. Loughran notes, “These areas are at the core of our service and operations, and are of vital importance to our clients. By passing the audit in all of these areas, we are able to validate to our clients that we manage and operate our data center with the utmost care commitment, and attention to detail.”</p>
