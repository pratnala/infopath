<?php
	include_once ("subnav.php");
	include_once ("../../inc/essentials.php");
?>

<script>
	$mainNav.set("Welcome");
</script>

<h1>Getting Started</h1>

<p align = "justify">Infopath's Managed Services’ distinctive characteristic is its ability to understand business and the use of technology to fit customers’ needs. By creating an atmosphere of consistent, high-quality customer service, Infopath's Managed Services provide, deliver and manage network-based services, applications, and equipment to enterprises.</p>

<table style = "float:left; padding:0 10px 10px 10px;">
<caption align="bottom"><strong>Come join us!</strong></caption>
<tr><td><img src="img/tilegroup_welcome/getting_started/joinus.jpg" height = "150px"/></td></tr>
</table>
<p align = "justify">Infopath has been on the forefront of managing complex infrastructures for our large carrier, enterprise network, data center, and hosting provider clients for over a decade. Our unique vendor-agnostic position has made us a trusted partner for organizations that operate in diverse vendor environments. Our unique expertise allows us to leverage our client’s legacy investments to meet today’s challenges.</p>

<p align = "justify">Infopath's Managed Services provides, delivers and manages network-based services, applications, and equipment to enterprises, residences, or other service providers. Infopath provides these services by hosting companies, network management, including advanced features like IP telephony, messaging and call center, virtual private networks (VPNs), managed firewalls, and monitoring/reporting of network servers. Most of these services can be performed from outside your company's internal network with a special emphasis placed on integration and certification of Internet security for applications and content.</p>

<p align = "justify">We understand that business and technology requirements differ. That is why Infopath offers a service continuum that provides customers with a full range of managed services. Some customers want to manage their platform in a secure environment but need to compliment their IT staff with one or more services. Other customers want to manage their content and leave the Data Center Services to Infopath. Whatever your need and wherever you fall in the service continuum, Infopath will architect the right solution for you and let you get back to the business of business.</p>
