<?PHP
date_default_timezone_set ('UTC');
/* CONFIG SETTINGS - change this */
$siteTitle = 'Infopath'; /* will be displayed above the url-bar */
$siteName = 'Infopath'; /* The biggest title on your homepage */
$siteDesc ='Data Center Service Assurance'; /*subtitle */
$siteTitleHome = 'Data Center Service Assurance'; // will be displayed above the url-bar when the home-page is open
$siteFooter = '<table><tr><td align="left"><strong>
			Infopath<br />
			Data Center Service Assurance<br />
			6825 Shiloh Road East, Alpharetta, Suite B-1, GA 30005-2227. Phone: 770-205-3960
			</strong></td>
			<td align="right">
			<a href="https://www.facebook.com/pages/Infopath/168955483142204"><img src="img/social/facebook.jpg" width="30px" /></a>
			<a href="https://twitter.com/infopath1"><img src="img/social/twitter.png" width="30px" /></a>
			<a href="skype:infopathcorp?call"><img src="img/social/skype.jpg" width="30px" /></a></td>
			<td>&#169; 2001 - ' . date ("Y"). ' Infopath<br />All Rights Reserved</td></tr></table>';

$siteMetaDesc = 'Infopath is uniquely and exclusively focused on helping our clients make smarter decisions about enhancing their Information Technology solutions.';
$siteMetaKeywords = 'infopath, data center, services';

$theme = "theme_default"; // name of the subfolder in themes directory for the theme you want

$enableMobileVersion = false; // go to mobi/js/tiles-mob.js to edit the tile config for mobile browsers after your main site is done!!! check the tutorial please!
$enableNoJavascript = false; // ONLY FOR MOBILE VERSION; adds some features for people that have no javascript. Set to TRUE when the mobile version of your site is finished. This will create a file no-js-mob.txt in the compress folder. Changes wont be visible until you delete that file and reload the page. Always reload the page yourself after deleting that file! IF YOU DO NOT REFRESH IMMEDIATLY THE MOBILE PAGE AFTER REMOVING NO-JS-MOB.txt, IT CAN BE A HUGE POTENTIAL LEAK.



/*Must be the same as the pageLink array in js/config.js ! For SEO optimization (tip: first create the array in config.js, then copy it here and place a $-sign in front of every line */
$pageLink = array();
$pageLink['Welcome'] = 'welcome/welcome.php';
$pageLink['Our Partnerships'] = 'welcome/partnerships.php';
$pageLink['Getting Started'] = 'welcome/getting_started.php';
$pageLink['Overview'] = 'about/overview.php';
$pageLink['What We Do'] = 'about/what_we_do.php';
$pageLink['Our Goals'] = 'about/our_goals.php';
$pageLink['Contact Us'] = 'about/contact_us.php';
$pageLink['Values'] = 'about/values.php';
$pageLink['Mission'] = 'about/mission.php';
$pageLink['Careers'] = 'about/careers.php';
$pageLink['What We Offer'] = 'services/what_we_offer.php';
$pageLink['Our Portfolio'] = 'services/portfolio.php';
$pageLink['Key Features'] = 'solutions/features.php';
$pageLink['Success Portfolio'] = 'solutions/success_portfolio.php';
$pageLink['Infopath Process'] = 'solutions/process.php';
$pageLink['SSAE'] = 'solutions/ssae.php';
$pageLink['Data Center Infrastructure Management'] = 'solutions/dcim.php';
$pageLink['Run Book'] = 'solutions/run_book.php';
$pageLink['Technology Lifecycle'] = 'solutions/technology_lifecycle.php';

/*YOUR LOGIN FOR ADMIN SECTION, access it on http:/yoursite.com/admin/ */
$adminLogin = 'user';
$adminPassword = 'pass';

/*COMPRESSING */
$enableCompressionCss =false; // compress and combine CSS files? See the admin section and tutorial on http://metro-webdesign.info before enabling this!
$enableCompressionJs =false; // compress and combine Javascript files? See the admin section and tutorial on http://metro-webdesign.info before enabling this!

$autoFlush = true; // automaticly flush the cache when compression of JS or CSS is on and you have edited a file? When set to false, you'll have to manually flush the cache when you edited a file. Set to FALSE on slow servers to speed up page loading.
$autoFlushPlugins = true; // automaticly flush cache when plugins are changed? If you set this to FALSE, it's a good speed boost, but you'll have to remember to manually flush the cache from the amdin section if you update/change/add/remove a plugin. Only works if $autoFlush is enabled
?>