$tile.scale = 145 // size of tiles to match the full width of the screen LEAVE IT LIKE THIS
$group.spacingMob = new Array(0,5,10); // defines the vertical space between the headers in tiles, each array index is a tilegroup, so you can specify different spaces for the different groups.
tilesMobile = function(){
	/* ADD YOUR TILES FOR MOBILE VERSION HERE!!! The best way to do this is to copy the tiles from your main function, and modify their position.
	 Tile template functions (the part below) from the normal tiles.js works on both version so should'nt be copied. (for example, you can just use tileTitleText() here, without modifications)
	 Also, the pages of your normal website will be used.  */
	
	/* READ THE TUTORIAL AT METRO-WEBDESIGN.INFO */
	/*Group 0 - Welcome*/
	tileImageSlider (0, 0, 0, 1, 1, '', 'Welcome', 'img/tilegroup_welcome/infopath.png', 1, '<span style="font-size:25px;"><br />Welcome to Infopath!</span>', 1, '', 'GradientWelcomeTile');
	tileHoverEffectFold (0, 1, 0, 2, 1, '', 'Our Partnerships', 'img/tilegroup_welcome/partnerships.jpg', 'Know more about our partnerships!', '', 'GradientPartnershipsTile');
	tileLive (0, 0, 1, 3, 1, '', 'Getting Started', 'Getting Started', 'img/tilegroup_welcome/getting-started.jpg', 145, 0, 0, 3000, ['<span style = "font-size:17px;">We believe in customer-focused services</span>','<span style = "font-size:17px;">Know more about how Infopath can understand your business and fit your needs</span>', '<span style = "font-size:17px;">Click here</span>'], '', 'GradientGettingStartedTile');
	
	/*Group 1 - About*/
	tileFlip (1, 0, 0, 1, 1, '', 'Overview', 'img/tilegroup_about/overview.jpg','<span style="font-size:25px;"><br />Overview</span>', '', 'GradientOverviewTile');
	tileHoverEffectLeft (1, 1, 0, 2, 1, '', 'What We Do', 'img/tilegroup_about/what-we-do.jpg', 'Find out more about what we do!', '', 'GradientWhatWeDoTile');
	tileLive (1, 0, 1, 3, 1, '', 'Our Goals', 'Our Goals', 'img/tilegroup_about/goal.png', 145, 0, 0, 3000, ['<span style = "font-size:17px;">Improve Platform</span>', '<span style = "font-size:17px;">Improve Process</span>', '<span style = "font-size:17px;">Improve People</span>'], '', 'GradientGoalsTile');
	
	/*Group 2 - Services*/
	tileImageSlider (2, 0, 0, 1, 1, '', 'What We Offer', 'img/tilegroup_services/what-we-offer.jpg', 1, '<br /><span style="font-size:20px;">Know more about the services we offer!</span>', 1, '', 'GradientServicesTile');
	tileHoverEffectRight (2, 1, 0, 2, 1, '', 'Our Portfolio', 'img/tilegroup_services/portfolio.jpg', 'Our Portfolio', '', 'GradientPortfolioTile');
	
	/*Group 3 - Solutions*/
	tileFlip (3, 0, 0, 1, 1, '', 'Key Features', 'img/tilegroup_solutions/key-features.jpg', '<br /><span style="font-size:25px;">Key Features</span>', '', 'GradientFeaturesTile');
	tileHoverEffectFold (3, 1, 0, 2, 1, '', 'Success Portfolio', 'img/tilegroup_solutions/success-portfolio.jpg', 'Click here to find about our success portfolio!', '', 'GradientSuccessTile');
	tileImg (3, 0, 1, 1, 1, '', 'Infopath Process', 'img/tilegroup_solutions/process.jpg', 1, 1, ['Process', '#88543C', 'bottom', '', true], 'GradientProcessTile');
	tileFlip (3, 1, 1, 1, 1, '', 'SSAE', 'img/tilegroup_solutions/ssae.jpg', '<br /><span style="font-size:24px;">SSAE 16 information</span>', '', 'GradientSSAETile');
	tileImageSlider (3, 2, 1, 1, 1, '', 'Data Center Infrastructure Management', 'img/tilegroup_solutions/dcim.jpg', 1, '<span style="font-size:15px;"><br />Enlighten yourselves on how we manage your Data Center Infrastructure</span>', 1, '', 'GradientDCIMTile');
	tileTitleTextImage (3, 0, 2, 2, 1, '', 'Run Book', 'Run Book', 'Know how we cope with data center complexity', 'img/tilegroup_solutions/datacenter.jpg', 145, 0, 0, '', 'GradientRunbookTile');
	tileFlip (3, 2, 2, 1, 1, '', 'Technology Lifecycle', 'img/tilegroup_solutions/lifecycle.jpg', '<br /><span style="font-size:24px;">Technology Lifecycle</span>', '', 'GradientLifecycleTile');
}

tileToMain = function(group,x,y,width,height,bg,optClass){ // makes a tile that redirects to our main site (in case someone needs the main site)
	$page.content += (
	"<a href='index.php' class='tile group"+group+" "+optClass+"' \
	onClick='javascript:if(!confirm(\"Are you sure you want to go to the main site?\")){return false;}else{setCookie(\"desktop\",1,7);}'\
	style=' \
	margin-top:"+((y*$tile.scalespacing)+45)+"px;margin-left:"+(x*$tile.scalespacing+group*$group.spacing)+"px; \
	width: "+(width*$tile.scalespacing-$tile.spacing)+"px; height:"+(height*$tile.scalespacing-$tile.spacing)+"px; \
	background:"+bg+";'>\
	<div class='tileTitle'>Welcome to our mobile site</div>\
	<div class='tileDesc' style='margin-top:10px;'>Click here to go to our desktop site</div>\
	</a>");
}
tileCopyright = function(group,x,y,width,height,text,optClass){ // makes a tile that redirects to our main site (in case someone needs the main site)
	$page.content += (
	"<a class='tile group"+group+" "+optClass+"' \
	style=' \
	margin-top:"+((y*$tile.scalespacing)+45)+"px;margin-left:"+(x*$tile.scalespacing+group*$group.spacing)+"px; \
	width: "+(width*$tile.scalespacing-$tile.spacing)+"px; height:"+(height*$tile.scalespacing-$tile.spacing)+"px;' \>\
	<div id='copyrightTileDesc'>© "+text+"</div>\
	</a>");
}

