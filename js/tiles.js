/*TILES*/
tiles = function(){ /*Insert your own tiles here*/

	/* Group 0 - Welcome */
	tileImageSlider (0, 0, 0, 1, 1, '', 'Welcome', 'img/tilegroup_welcome/welcome.gif', 1, '<span style = "font-size: 25px;"><br />Welcome to Infopath!</span>', 1, '', 'GradientWelcomeTile');
	tileHoverEffectFold (0, 1, 0, 2, 1, '', 'Our Partnerships', 'img/tilegroup_welcome/partnerships.jpg', '<span style = "font-size: 19px;">Know more about our partners!</span>', '', 'GradientPartnershipsTile');
	tileLive (0, 0, 1, 3, 1, '', 'Getting Started', 'Getting Started', 'img/tilegroup_welcome/getting-started.jpg', 145, 0, 0, 3000, ['<span style = "font-size: 17px;">We believe in customer-focused services</span>','<span style = "font-size: 17px;">Know more about how Infopath can understand your business and fit your needs</span>', '<span style = "font-size: 17px;">Click here</span>'], '', 'GradientGettingStartedTile');
	
	/* Group 1 - About */
	tileFlip (1, 0, 0, 1, 1, overviewBg, 'Overview', 'img/tilegroup_about/overview.jpg', '<span style = "font-size: 25px;"><br />Overview</span>', '', '');
	tileHoverEffectLeft (1, 1, 0, 2, 1, '', 'What We Do', 'img/tilegroup_about/what-we-do.jpg', '<span style = "font-size: 19px;">Find out more about what we do!</span>', '', 'GradientWhatWeDoTile');
	tileLive (1, 0, 1, 2, 1, '', 'Our Goals', 'Our Goals', 'img/tilegroup_about/goal.png', 145, 0, 0, 3000, ['<span style = "font-size: 17px;">Improve Platform</span>', '<span style = "font-size:17px;">Improve Process</span>', '<span style = "font-size: 17px;">Improve People</span>'], '', 'GradientGoalsTile');
	tileImageSlider (1, 2, 1, 1, 1, '', 'Contact Us', 'img/tilegroup_about/contact-us.jpg', 1, '<span style="font-size:20px;"><br />Click here to find out ways to reach us!</span>', 1, '', 'GradientContactUsTile');
	tileImg (1, 0, 2, 1, 1, '#880000', 'Mission', 'img/tilegroup_about/mission.jpg', 1, 1, ['Mission', '#4321FA', 'bottom', '', true], '');
	tileImageSlider (1, 1, 2, 1, 1, '#A08C29', 'Values', 'img/tilegroup_about/values.gif', 1, '<span style = "font-size: 20px;"><br />The Infopath Value System</spam>', 1, '', '');
	tileFlip (1, 2, 2, 1, 1, '#C16B32', 'Careers', 'img/tilegroup_about/careers.jpg', '<br /><span style = "font-size: 25px;">Careers @ Infopath</span>', '', '');
	
	/* Group 2 - Services */
	tileImageSlider (2, 0, 0, 1, 1, '', 'What We Offer', 'img/tilegroup_services/what-we-offer.jpg', 1, '<br /><span style = "font-size: 20px;">Know more about the services we offer!</span>', 1, '', 'GradientServicesTile');
	tileHoverEffectRight (2, 1, 0, 2, 1, '', 'Our Portfolio', 'img/tilegroup_services/portfolio.jpg', '<span style = "font-size: 20px;">Our Portfolio</span>', '', 'GradientPortfolioTile');
	
	/* Group 3 - Solutions */
	tileFlip (3, 0, 0, 1, 1, featuresBg, 'Key Features', 'img/tilegroup_solutions/key-features.jpg', '<br /><span style = "font-size: 25px;">Key Features</span>', '', '');
	tileHoverEffectFold (3, 1, 0, 2, 1, '', 'Success Portfolio', 'img/tilegroup_solutions/success-portfolio.jpg', '<span style = "font-size: 17px;">Click here to find about our success portfolio!</span>', '', 'GradientSuccessTile');
	tileImg (3, 0, 1, 1, 1, '#FF1929', 'Infopath Process', 'img/tilegroup_solutions/process.jpg', 1, 1, ['Process', '#88543C', 'bottom', '', true], '');
	tileFlip (3, 1, 1, 1, 1, SSAEBg, 'SSAE', 'img/tilegroup_solutions/ssae.jpg', '<br /><span style = "font-size: 24px;">SSAE 16 information</span>', '', '');
	tileImageSlider (3, 2, 1, 1, 1, '', 'Data Center Infrastructure Management', 'img/tilegroup_solutions/dcim.jpg', 1, '<span style = "font-size: 15px;"><br />Enlighten yourselves on how we manage your Data Center Infrastructure</span>', 1, '', 'GradientDCIMTile');
	tileTitleTextImage (3, 0, 2, 2, 1, '', 'Run Book', 'Run Book', 'Know how we cope with data center complexity', 'img/tilegroup_solutions/datacenter.jpg', 145, 0, 0, '', 'GradientRunbookTile');
	tileFlip (3, 2, 2, 1, 1, lifecycleBg, 'Technology Lifecycle', 'img/tilegroup_solutions/lifecycle.jpg', '<br /><span style = "font-size: 24px;">Technology Lifecycle</span>', '', '');
	
	/* Group 4 - Support */
	tileImg (4, 0, 0, 1, 1, '#210101', '', 'img/tilegroup_support/support.jpg', 1, 1, ['Welcome to', '#99219A', 'bottom', 'Infopath Support', false], '');
	tileFlip (4, 1, 0, 1, 1, remoteBg, '', 'img/tilegroup_support/remote-access.png', '<br /><span style = "font-size: 21px;">Remote Access Services</span>', '', '');
	tileImageSlider (4, 2, 0, 1, 1, '', '', 'img/tilegroup_support/live-chat.jpg', 1, '<span style = "font-size: 12px;">Have a live chat with our support representative</span>', 0.3, '', 'GradientLiveTile');
	tileHoverEffectRight (4, 0, 1, 2, 1, '', '', 'img/tilegroup_support/data-center.jpg', '<span style = "font-size: 19px;">Onsite Data Center Services</span>', '', 'GradientOnsiteTile');
	tileFlip (4, 2, 1, 1, 1, conferenceBg, '', 'img/tilegroup_support/conference.gif', '<br /><span style = "font-size: 21px;">Video Conferencing Center</span>', '', '');
	tileFlip (4, 0, 2, 1, 1, '#CEBA06', '', 'img/tilegroup_support/policy.jpg', '<br /><span style = "font-size:20px;">Our Support Policy</span>', '', '');
	tileHoverEffectFold (4, 1, 2, 2 ,1 , '', '', 'img/tilegroup_support/vpn.png', 'VPN Connection to Support', '', 'GradientVPNTile');
}

/*Tile Templates */
tileTitleText = function(group,x,y,width,height,bg,linkPage,title,text,labelSettings,optClass){ /* Tile with only a title and description */	
	if(labelSettings!='' && labelSettings[0] != ''){
		var label=labelSettings[0];
		var labelcolor=labelSettings[1];
		var labelposition=labelSettings[2];
		if(labelposition=='top'){
			var labelText ="<div class='tileLabelWrapper top' style='border-top-color:"+labelcolor+";'><div class='tileLabel top' >"+label+"</div></div>";
		}else{
			var labelText ="<div class='tileLabelWrapper bottom'><div class='tileLabel bottom' style='border-bottom-color:"+labelcolor+";'>"+label+"</div></div>";
		}
	}else{
		labelText='';
	}
	$page.content += (
	"<a "+makeLink(linkPage)+" class='tile group"+group+" "+optClass+"' style=' \
	margin-top:"+((y*$tile.scalespacing)+45)+"px; margin-left:"+(x*$tile.scalespacing+group*$group.spacing)+"px; \
	width: "+(width*$tile.scalespacing-$tile.spacing)+"px; height:"+(height*$tile.scalespacing-$tile.spacing)+"px; \
	background:"+bg+";'>\
	<div class='tileTitle'>"+title+"</div>\
	<div class='tileDesc'>"+text+"</div>\
	"+labelText+"\
	</a>");
}
tileImg = function(group,x,y,width,height,bg,linkPage,img,imgSizeWidth,imgSizeHeight,labelSettings,optClass){
	if(labelSettings!='' && labelSettings[0] != ''){
		var id= "tileimg_"+(group+''+x+''+y).replace(/\./g,'_')
		var label=labelSettings[0];
		var labelcolor=labelSettings[1];
		var labelposition=labelSettings[2];
		var desc=labelSettings[3];
		var showDescOnHover=labelSettings[4];		
		var displayLabel = (showDescOnHover) ? " showOnHover": ""; 
		var labelDescText = (desc!='') ? "<div class='tileLabelDesc "+displayLabel+"'>"+desc+"</div>" : '';	
		if(labelposition=='top'){
			var totalLabel ="<div class='tileLabelWrapper top' style='border-top-color:"+labelcolor+";'><div class='tileLabel top' >"+label+"</div>"+labelDescText+"</div>";
		}else{
			
			var totalLabel ="<div class='tileLabelWrapper bottom'><div class='tileLabel bottom' style='border-bottom-color:"+labelcolor+";'>"+label+"</div>"+labelDescText+"</div>";
		}		
		$(function(){
			$.plugin($afterTilesAppend,{
				tileImg:function(){
					var id2 = id;
					if(labelposition=='top'){
						$("."+id2).bind("mouseenter",function(){$(this).find("div.showOnHover").show(200);});
						$("."+id2).bind("mouseleave",function(){$(this).find("div.showOnHover").stop().hide(200);});
					}else{
						$("."+id2).bind("mouseenter",function(){$(this).find("div.showOnHover").css("bottom",0).slideDown(200);});
						$("."+id2).bind("mouseleave",function(){$(this).find("div.showOnHover").css("top",0).stop().slideUp(200);});	
					}
				}
			});
		});
	}else{
		var id="";
		var totalLabel = "";
	}
	var drawHeight = (imgSizeWidth*$tile.scalespacing-$tile.spacing)
	var drawWidth = (imgSizeHeight*$tile.scalespacing-$tile.spacing)
	var tileHeight = (height*$tile.scalespacing-$tile.spacing)
	var tileWidth = (width*$tile.scalespacing-$tile.spacing)
	$page.content += ("<a "+makeLink(linkPage)+" class='tile tileImg group"+group+" "+id+" "+optClass+"' style=' \
	margin-top:"+((y*$tile.scalespacing)+45)+"px ;margin-left:"+(x*$tile.scalespacing+group*$group.spacing)+"px; \
	width: "+tileWidth+"px; height:"+tileHeight+"px; \
	background:"+bg+";'>\
	<img src='"+img+"' width="+drawWidth+" height="+drawHeight+" \
	style='margin-left: "+((tileWidth-drawWidth)*0.5)+"px; \
	margin-top: "+((tileHeight-drawHeight)*0.5)+"px'/>\
	"+totalLabel+"\
	</a>");
}

tileTitleTextImage = function(group,x,y,width,height,bg,linkPage,title,text,img,imgSize,imgToTop,imgToLeft,labelSettings,optClass){ // Tile with an image on the left side, a title, and a description, width is always 2
	if(labelSettings!='' && labelSettings[0] != ''){
		var label=labelSettings[0];
		var labelcolor=labelSettings[1];
		var labelposition=labelSettings[2];
		if(labelposition=='top'){
			var labelText ="<div class='tileLabelWrapper top' style='border-top-color:"+labelcolor+";'><div class='tileLabel top' >"+label+"</div></div>";
		}else{
			var labelText ="<div class='tileLabelWrapper bottom'><div class='tileLabel bottom' style='border-bottom-color:"+labelcolor+";'>"+label+"</div></div>";
		}
	}else{
		labelText='';
	}
	$page.content += (
	"<a "+makeLink(linkPage)+" class='tile group"+group+" "+optClass+"' style=' \
	margin-top:"+((y*$tile.scalespacing)+45)+"px;margin-left:"+(x*$tile.scalespacing+group*$group.spacing)+"px; \
	width: "+(width*$tile.scalespacing-$tile.spacing)+"px; height:"+(height*$tile.scalespacing-$tile.spacing)+"px; \
	background:"+bg+";'>\
	<img style='float:left; margin-top:"+imgToTop+"px;margin-left:"+imgToLeft+"px;' src='"+img+"' height="+imgSize+" width="+imgSize+"/> \
	<div class='tileTitle' style='margin-left:"+(imgSize+5+imgToLeft)+"px;'>"+title+"</div>\
	<div class='tileDesc' style='margin-left:"+(imgSize+6+imgToLeft)+"px;'>"+text+"</div>\
	"+labelText+"\
	</a>");
}
tileLive = function(group,x,y,width,height,bg,linkPage,title,img,imgSize,imgToTop,imgToLeft,speed,textArray,labelSettings,optClass){
	if(labelSettings!='' && labelSettings[0] != ''){
		var label=labelSettings[0];
		var labelcolor=labelSettings[1];
		var labelposition=labelSettings[2];
		if(labelposition=='top'){
			var labelText ="<div class='tileLabelWrapper top' style='border-top-color:"+labelcolor+";'><div class='tileLabel top' >"+label+"</div></div>";
		}else{
			var labelText ="<div class='tileLabelWrapper bottom'><div class='tileLabel bottom' style='border-bottom-color:"+labelcolor+";'>"+label+"</div></div>";
		}
	}else{
		labelText='';
	}
	var id= "livetile_"+(group+''+x+''+y).replace(/\./g,'_')
	if(img!=''){
		imgInsert = "<img style='float:left; margin-top:"+imgToTop+"px;margin-left:"+imgToLeft+"px;' src='"+img+"' height="+imgSize+" width="+imgSize+"/>"
	}else{
		imgInsert = '';
		imgSize = 0;
		imgToLeft = 0;
	}
	$page.content += (
	"<a "+makeLink(linkPage)+" class='tile group"+group+" "+optClass+"' style=' \
	margin-top:"+((y*$tile.scalespacing)+45)+"px; margin-left:"+(x*$tile.scalespacing+group*$group.spacing)+"px; \
	width: "+(width*$tile.scalespacing-$tile.spacing)+"px; height:"+(height*$tile.scalespacing-$tile.spacing)+"px; \
	background:"+bg+";'>\
	"+imgInsert+"\
	<div class='tileTitle' style='margin-left:"+(imgSize+5+imgToLeft)+"px;'>"+title+"</div>\
	<div class='livetile' style='margin-left:"+(imgSize+10+imgToLeft)+"px;' id='"+id+"' >"+textArray[0]+"</div>\
	"+labelText+"\
	</a>");
	setTimeout(function(){newLiveTile(id,group,textArray,speed,0)},speed); // init this tile
}
tileImageSlider = function(group,x,y,width,height,bg,linkPage,img,imgsize, text,slideDistance,labelSettings,optClass){
	if(labelSettings!='' && labelSettings[0] != ''){
		var label=labelSettings[0];
		var labelcolor=labelSettings[1];
		var labelposition=labelSettings[2];
		if(labelposition=='top'){
			var labelText ="<div class='tileLabelWrapper top' style='border-top-color:"+labelcolor+";'><div class='tileLabel top' >"+label+"</div></div>";
		}else{
			var labelText ="<div class='tileLabelWrapper bottom'><div class='tileLabel bottom' style='border-bottom-color:"+labelcolor+";'>"+label+"</div></div>";
		}
	}else{
		labelText='';
	}
	tileWidth = width*$tile.scalespacing-$tile.spacing
	tileHeight = height*$tile.scalespacing-$tile.spacing
	$page.content += ("<a "+makeLink(linkPage)+" class='tile group"+group+" "+optClass+" tileImageSlider' id='"+slideDistance+" ' style=' \
	margin-top:"+((y*$tile.scalespacing)+45)+"px;margin-left:"+(x*$tile.scalespacing+group*$group.spacing)+"px; \
	width: "+tileWidth+"px; height:"+tileHeight+"px; \
	background:"+bg+"; text-align:center;'>\
	<div class='tileImageSliderWrapper' style='position:absolute;'>\
	<div style='width: "+tileWidth+"px; height:"+tileHeight+"px;'>\
	<img src='"+img+"' height="+tileHeight*imgsize+" style='margin-top: "+((tileHeight-tileHeight*imgsize)*0.5)+"px'/>\
	</div>\
	<div text='tileImageSliderText'>"+text+"</div>\
	"+labelText+"\
	</div>\
	</a>");
	$(document).on("mouseover",'.tileImageSlider',function(){
		$(this).find(".tileImageSliderWrapper").stop(true,false).animate({"margin-top":-$(this).height()*$(this).attr("id")},250,"linear");
	}).on("mouseleave",'.tileImageSlider',function(){
		$(this).find(".tileImageSliderWrapper").stop(true,false).animate({'margin-top':0},300,"linear");
	});
}
tileCustom = function(group,x,y,width,height,bg,linkPage,content,labelSettings,optClass){ // make your own tiles
	if(labelSettings!='' && labelSettings[0] != ''){
		var label=labelSettings[0];
		var labelcolor=labelSettings[1];
		var labelposition=labelSettings[2];
		if(labelposition=='top'){
			var labelText ="<div class='tileLabelWrapper top' style='border-top-color:"+labelcolor+";'><div class='tileLabel top' >"+label+"</div></div>";
		}else{
			var labelText ="<div class='tileLabelWrapper bottom'><div class='tileLabel bottom' style='border-bottom-color:"+labelcolor+";'>"+label+"</div></div>";
		}
	}else{
		labelText='';
	}
	$page.content += (
	"<a "+makeLink(linkPage)+" class='tile group"+group+" "+optClass+"' style=' \
	margin-top:"+((y*$tile.scalespacing)+45)+"px;margin-left:"+(x*$tile.scalespacing+group*$group.spacing)+"px; \
	width: "+(width*$tile.scalespacing-$tile.spacing)+"px; height:"+(height*$tile.scalespacing-$tile.spacing)+"px; \
	background:"+bg+";'>\
	"+content+"\
	"+labelText+"\
	</a>");
}

overviewBg = "background: rgb(0,36,0);background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMjQwMCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjE3JSIgc3RvcC1jb2xvcj0iIzAwNTcwMCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjMzJSIgc3RvcC1jb2xvcj0iIzAwOGEwMCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjY3JSIgc3RvcC1jb2xvcj0iIzUyYjE1MiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);background: -moz-linear-gradient(top,  rgba(0,36,0,1) 0%, rgba(0,87,0,1) 17%, rgba(0,138,0,1) 33%, rgba(82,177,82,1) 67%);background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,36,0,1)), color-stop(17%,rgba(0,87,0,1)), color-stop(33%,rgba(0,138,0,1)), color-stop(67%,rgba(82,177,82,1)));background: -webkit-linear-gradient(top,  rgba(0,36,0,1) 0%,rgba(0,87,0,1) 17%,rgba(0,138,0,1) 33%,rgba(82,177,82,1) 67%);background: -o-linear-gradient(top,  rgba(0,36,0,1) 0%,rgba(0,87,0,1) 17%,rgba(0,138,0,1) 33%,rgba(82,177,82,1) 67%);background: -ms-linear-gradient(top,  rgba(0,36,0,1) 0%,rgba(0,87,0,1) 17%,rgba(0,138,0,1) 33%,rgba(82,177,82,1) 67%);background: linear-gradient(to bottom,  rgba(0,36,0,1) 0%,rgba(0,87,0,1) 17%,rgba(0,138,0,1) 33%,rgba(82,177,82,1) 67%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#002400', endColorstr='#52b152',GradientType=0 );"

featuresBg = "background: rgb(255,127,4);background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMTAwJSIgeDI9IjEwMCUiIHkyPSIwJSI+CiAgICA8c3RvcCBvZmZzZXQ9IjAlIiBzdG9wLWNvbG9yPSIjZmY3ZjA0IiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iNTAlIiBzdG9wLWNvbG9yPSIjZmZhNzNkIiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iI2ZmYjc2YiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);background: -moz-linear-gradient(45deg,  rgba(255,127,4,1) 0%, rgba(255,167,61,1) 50%, rgba(255,183,107,1) 100%);background: -webkit-gradient(linear, left bottom, right top, color-stop(0%,rgba(255,127,4,1)), color-stop(50%,rgba(255,167,61,1)), color-stop(100%,rgba(255,183,107,1)));background: -webkit-linear-gradient(45deg,  rgba(255,127,4,1) 0%,rgba(255,167,61,1) 50%,rgba(255,183,107,1) 100%);background: -o-linear-gradient(45deg,  rgba(255,127,4,1) 0%,rgba(255,167,61,1) 50%,rgba(255,183,107,1) 100%);background: -ms-linear-gradient(45deg,  rgba(255,127,4,1) 0%,rgba(255,167,61,1) 50%,rgba(255,183,107,1) 100%);background: linear-gradient(45deg,  rgba(255,127,4,1) 0%,rgba(255,167,61,1) 50%,rgba(255,183,107,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff7f04', endColorstr='#ffb76b',GradientType=1 );"

SSAEBg = "background: rgb(30,87,153);background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIxMDAlIiB5Mj0iMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzFlNTc5OSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUwJSIgc3RvcC1jb2xvcj0iIzI5ODlkOCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUxJSIgc3RvcC1jb2xvcj0iIzIwN2NjYSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiM3ZGI5ZTgiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);background: -moz-linear-gradient(left,  rgba(30,87,153,1) 0%, rgba(41,137,216,1) 50%, rgba(32,124,202,1) 51%, rgba(125,185,232,1) 100%);background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(30,87,153,1)), color-stop(50%,rgba(41,137,216,1)), color-stop(51%,rgba(32,124,202,1)), color-stop(100%,rgba(125,185,232,1)));background: -webkit-linear-gradient(left,  rgba(30,87,153,1) 0%,rgba(41,137,216,1) 50%,rgba(32,124,202,1) 51%,rgba(125,185,232,1) 100%);background: -o-linear-gradient(left,  rgba(30,87,153,1) 0%,rgba(41,137,216,1) 50%,rgba(32,124,202,1) 51%,rgba(125,185,232,1) 100%);background: -ms-linear-gradient(left,  rgba(30,87,153,1) 0%,rgba(41,137,216,1) 50%,rgba(32,124,202,1) 51%,rgba(125,185,232,1) 100%);background: linear-gradient(to right,  rgba(30,87,153,1) 0%,rgba(41,137,216,1) 50%,rgba(32,124,202,1) 51%,rgba(125,185,232,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1e5799', endColorstr='#7db9e8',GradientType=1 );"

lifecycleBg = "background: rgb(98,125,77);background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzYyN2Q0ZCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMxZjNiMDgiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);background: -moz-linear-gradient(top,  rgba(98,125,77,1) 0%, rgba(31,59,8,1) 100%);background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(98,125,77,1)), color-stop(100%,rgba(31,59,8,1)));background: -webkit-linear-gradient(top,  rgba(98,125,77,1) 0%,rgba(31,59,8,1) 100%);background: -o-linear-gradient(top,  rgba(98,125,77,1) 0%,rgba(31,59,8,1) 100%);background: -ms-linear-gradient(top,  rgba(98,125,77,1) 0%,rgba(31,59,8,1) 100%);background: linear-gradient(to bottom,  rgba(98,125,77,1) 0%,rgba(31,59,8,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#627d4d', endColorstr='#1f3b08',GradientType=0 );"

remoteBg = "background: #ffa84c;background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZmYTg0YyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNmZjdiMGQiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);background: -moz-linear-gradient(top,  #ffa84c 0%, #ff7b0d 100%);background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffa84c), color-stop(100%,#ff7b0d));background: -webkit-linear-gradient(top,  #ffa84c 0%,#ff7b0d 100%);background: -o-linear-gradient(top,  #ffa84c 0%,#ff7b0d 100%);background: -ms-linear-gradient(top,  #ffa84c 0%,#ff7b0d 100%);background: linear-gradient(to bottom,  #ffa84c 0%,#ff7b0d 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffa84c', endColorstr='#ff7b0d',GradientType=0 );"

conferenceBg = "background: #6199c7;background: -moz-radial-gradient(center, ellipse cover,  #6199c7 50%, #26558b 100%);background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(50%,#6199c7), color-stop(100%,#26558b));background: -webkit-radial-gradient(center, ellipse cover,  #6199c7 50%,#26558b 100%);background: -o-radial-gradient(center, ellipse cover,  #6199c7 50%,#26558b 100%);background: -ms-radial-gradient(center, ellipse cover,  #6199c7 50%,#26558b 100%);background: radial-gradient(ellipse at center,  #6199c7 50%,#26558b 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#6199c7', endColorstr='#26558b',GradientType=1 );"