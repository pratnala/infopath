<div id="header">
	<div id = "logo">
		<img style = "width:60px;height:60px;" src="img/navbar/infopath.png" />
	</div>
	<div id="headerTitles">
		<h1><a href=""><?php echo $siteName?></a></h1>
   		<h2><?php echo $siteDesc;?></h2>
    </div>
    <div id="nav">
		<?php echo $plNav;
		/* Look at this to edit your own titles, the href is from your url bar when you scroll to the corresponding tilegroup, it's the part after the # with the # included. */?>
		<a id="welcome" class="navItem"  href="#&welcome">
			<img src="img/navbar/welcome.png"/><br />
			Welcome
		</a>
		<a id="about" class="navItem" href="#&about">
			<img src="img/navbar/aboutus.png"/><br />
			About
		</a>
		<a id="services" class="navItem" href="#&services">
			<img src="img/navbar/services.png"/><br />
			Services
		</a>
		<a id="solutions" class="navItem" href="#&solutions">
			<img src="img/navbar/solutions.png"/><br />
			Solutions
		</a>
		<a id="support" class="navItem" href="#&support">
			<img src="img/navbar/support.png"/><br />
			Support
		</a>
	</div>
</div>