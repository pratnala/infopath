This is the new website for infopath which is currently hosted here: http://www.infopath.net
This new site will be visible at http://www.cse.iitb.ac.in/~pratnala/infopath for the duration of the development.
It will be updated only when I update the repo there so changes might lag but the repo will be always updated.
So, clone the repo to see how it looks now.